export interface Logs {
  id?: number | string;
  turn?: number;
  person: string;
  mensage: string;
  color: string;
  style: string;
}

export interface RootObject {
  logs: Logs[];
}
