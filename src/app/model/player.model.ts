import { ObjectId } from "mongodb";
export interface RootObject {
    Player : Player[];
}

export interface Player {
  _id?: ObjectId;
  name: string;
  score: string;
  date?: Date;
}
