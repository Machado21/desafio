import { Player } from './../model/player.model';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ObjectId } from 'mongodb';
import axios from 'axios';

@Injectable({
  providedIn: 'root',
})
export class RanksService {
  private ranks: any[];

  constructor(private http: HttpClient) {
    fetch('https://nodejs-api-logistica.onrender.com', {
      method: 'GET',
    })
      .then((response) => {})
      .catch((error) => {});

    this.ranks = [];
  }

  url = 'http://localhost:3000/';
  urlOnline = 'https://nodejs-api-logistica.onrender.com/';

  getAllRank = async (): Promise<Observable<Player[]>> => {
    return this.http.get<Player[]>(this.urlOnline + 'ranking');
  };

  postRank = async (player: Player): Promise<Observable<any>> => {
    try {
      const response = await axios.post(this.urlOnline + 'ranking', player, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
      return response.data;
    } catch (error) {
      throw error;
    }
  };

  get rank() {
    return this.ranks;
  }

  addRank(player: Player) {
    this.hidratar(player);
    //sort by score
    this.ranks.push(player);
    return this.ranks.sort((a, b) => {
      return b.score - a.score;
    });
  }

  private hidratar(player: any) {
    player.data = new Date();
  }
}
