import { Component, OnInit } from '@angular/core';
import { RanksService } from '../services/ranks.service';
import { Player } from '../model/player.model';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.less'],
})
export class RankingComponent implements OnInit {
  ranks: Player[] = [];

  constructor(private service: RanksService) {}

  ngOnInit(): void {
    this.getRanks();
  }

  async getRanks() {
    (await this.service.getAllRank()).subscribe((ranks: Player[]) => {
      this.ranks = ranks; //
      this.ranks.sort((a, b) => {
        if (a.score < b.score) {
          return 1;
        }
        if (a.score > b.score) {
          return -1;
        }
        return 0;
      });
      
    });
  }
}
