import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Logs } from '../model/logs.model';
import { Player } from '../model/player.model';
import { RanksService } from '../services/ranks.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.less'],
})
export class GameComponent implements OnInit {
  logs: Logs[];
  turn: number;
  stun: boolean;
  endGame: boolean;
  msgEnd: string;
  msgEndLose: string;

  player: number;
  monster: number;

  playerName: string;

  attackSpecial: number;
  specialState: boolean;

  playerLife: String;
  monsterLife: String;

  barColorPlayer: String;
  barColorMonster: String;

  constructor(private service: RanksService, private router: Router) {
    this.logs = [];
    this.turn = 1;
    this.stun = false;
    this.endGame = false;
    this.msgEnd = 'end-closed';
    this.msgEndLose = 'end-closed';

    this.player = 100;
    this.monster = 100;

    this.playerName = '';

    this.attackSpecial = 0;
    this.specialState = false;

    this.playerLife = this.player + '%';
    this.monsterLife = this.monster + '%';

    this.barColorPlayer = '#08d42a';
    this.barColorMonster = '#08d42a';
  }

  attack() {
    if (!this.endGame) {
      const damage = this.getRandomInt(5, 10);
      if (this.monster > 0 && this.monster >= damage) {
        this.monster = this.monster - damage;
        this.addLog(
          this.turn,
          'player',
          `Jogador - Ataque Básico (-${damage})`,
          'attack'
        );
      } else if (this.monster <= damage) {
        this.monster = 0;
        this.addLog(
          this.turn,
          'player',
          `Jogador - Ataque Básico (-${damage})`,
          'attack'
        );
        this.win();
      }

      this.monsterLife = this.monster + '%';
      this.monsterAttack();
    }
  }

  specialAttack() {
    if (!this.endGame && this.attackSpecial == 0) {
      const damage = this.getRandomInt(10, 20);
      if (this.monster > 0 && this.monster >= damage) {
        this.monster = this.monster - damage;
        this.addLog(
          this.turn,
          'player',
          `Jogador - Ataque Especial (-${damage})`,
          'attack'
        );

        if (this.getRandomInt(1, 2) == 1) {
          this.stun = true;
          this.addLog(
            this.turn,
            'monster',
            `Monstro - Ficou Atordoado! `,
            'attack'
          );
        }
      } else if (this.monster <= damage) {
        this.monster = 0;
        this.addLog(
          this.turn,
          'player',
          `Jogador - Ataque Especial (-${damage})`,
          'attack'
        );
        this.win();
      }
      this.monsterLife = this.monster + '%';
      this.monsterAttack();
      this.attackSpecial += 2;
      this.specialState = true;
      this.stun = false;
    }
  }

  healing() {
    if (!this.endGame) {
      const heal = this.getRandomInt(5, 15);

      if (this.player > 0 && this.player + heal < 100) {
        this.player = this.player + heal;
      } else if ((this.player = 100 || this.player + heal > 100)) {
        this.player = 100;
      }
      this.playerLife = this.player + '%';
      this.addLog(
        this.turn,
        'player',
        `Jogador - Usou Cura (+${heal})`,
        'heal'
      );

      this.monsterAttack();
    }
  }

  giveUp() {
    if (confirm('Tem certeza que deseja desistir?')) {
      this.router.navigateByUrl('home');
    }
  }

  monsterAttack() {
    if (!this.stun && this.monster > 0) {
      if (this.turn % 4 != 0 && this.turn != 0) {
        const damage1 = this.getRandomInt(6, 12);
        if (this.player > 0 && this.player >= damage1) {
          this.player = this.player - damage1;
          this.addLog(
            this.turn,
            'monster',
            `Monstro - Ataque Básico (-${damage1})`,
            'attack'
          );
        } else if (this.player <= damage1) {
          this.player = 0;
          this.addLog(
            this.turn,
            'monster',
            `Monstro - Ataque Básico (-${damage1})`,
            'attack'
          );
          this.lose();
        }
      } else {
        const damage2 = this.getRandomInt(8, 16);
        if (this.player > 0 && this.player >= damage2) {
          this.player = this.player - damage2;
          this.addLog(
            this.turn,
            'monster',
            `Monstro - Ataque Especial (-${damage2})`,
            'attack'
          );
        } else if (this.player <= damage2) {
          this.player = 0;
          this.addLog(
            this.turn,
            'monster',
            `Monstro - Ataque Especial (-${damage2})`,
            'attack'
          );
          this.lose();
        }
      }
    }

    this.playerLife = this.player + '%';
    this.attackSpecial == 0 ? this.attackSpecial : this.attackSpecial--;
    this.attackSpecial == 0
      ? (this.specialState = false)
      : (this.specialState = true);
    this.lifeBar();
    this.turn++;
  }

  lifeBar() {
    this.player < 50 ? (this.barColorPlayer = '#d4c308') : this.barColorPlayer;
    this.player < 20 ? (this.barColorPlayer = '#d40808') : this.barColorPlayer;
    this.monster < 50
      ? (this.barColorMonster = '#d4c308')
      : this.barColorMonster;
    this.monster < 20
      ? (this.barColorMonster = '#d40808')
      : this.barColorMonster;
  }

  win() {
    this.endGame = true;
    this.addLog(this.turn, 'player', 'Jogador - Venceu!', 'win');

    this.msgEnd = 'end-opened';
  }

  lose() {
    this.endGame = true;
    this.addLog(this.turn, 'monster', 'Monstro - Venceu!', 'lose');
    this.msgEndLose = 'end-opened-lose';
  }

  points() {
    var result: number = (this.player * 1000) / this.turn;
    return Math.round(result);
  }

  reset() {
    const points = this.points();
    const emitterValue: Player = {
      name: this.playerName,
      score: points.toString(),
    };

    this.logs = [];
    this.turn = 1;
    this.stun = false;
    this.endGame = false;
    this.msgEnd = 'end-closed';
    this.msgEndLose = 'end-closed';

    this.player = 100;
    this.monster = 100;

    this.playerName = '';

    this.attackSpecial = 0;
    this.specialState = false;

    this.playerLife = this.player + '%';
    this.monsterLife = this.monster + '%';

    this.barColorPlayer = '#08d42a';
    this.barColorMonster = '#08d42a';
  }

  send() {
    const points = this.points();
    const emitterValue: Player = {
      name: this.playerName,
      score: points.toString(),
    };

    this.service.postRank(emitterValue);

    this.router.navigateByUrl('ranking');
  }

  addLog(turn: number, person: string, mensage: string, color: string) {
    this.logs.push({
      turn: turn,
      person: person,
      mensage: mensage,
      style: person == 'monster' ? 'end' : 'start',
      color:
        color == 'heal' ? '#51e872' : color == 'attack' ? '#e86551' : '#d4c308',
    });
  }

  //Function to randomize
  getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  ngOnInit(): void {}
}
